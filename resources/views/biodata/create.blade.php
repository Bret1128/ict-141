@extends('layouts.app')
@section('content')
	<div class="container">
		<div class="row">
			<div class="col-lg-10">
				<h3>New Biodata</h3>
			</div>
		</div>
	</div>

	@if ($errors->any())
		<div class="alert alert-danger">
			<strong>Whoops!</strong> there is something wrong with your input.<br>
			<ul>
				@foreach ($errors as $error)
				<li>{{$error}}</li>
				@endforeach
			</ul>
		</div>
		@endif

		<form action="{{route('biodata.store')}}" method="post">
			@csrf
			<div class="row">
				<div class="col-md-4">
					<strong>Name:</strong>
					<input type="text" name="name" class="form-control" placeholder="name">
				</div>
				<div class="col-md-9">
					<strong>Address:</strong>
					<textarea class="form-control" placeholder="Address" name="address" rows="5" cols="50"></textarea>
				</div>
				<div class="col-md-12">
					<a href="{{route('biodata.index')}}" class="btn btn-sm btn-success">Back</a>
					<button type="submit" class="btn btn-sm btn-primary">Submit</button>
				</div>
			</div>
		</form>
		@endsection

<!--   Bret -->