@extends('layouts.app')
@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-10">
			<h3>List of Books</h3>
		</div>
		<div class="col-sm-2">
			<a class="btn btn-sm btn-success" href="{{ route('book.create') }}">Create new Book</a>
		</div>
	</div>
	
	@if ($message = Session::get('success'))
	<div class="alert alert-success">
		<p>{{$message}}</p>
	</div>
	@endif

	<table class="table table-hover table-sm">
		<tr>
			<th width = "50px"><b>No.</b></th>
			<th width = "300px"><b>Name</b></th>
			<th width = "300px"><b>ISBN</b></th>
			<th>Details</th>
			<th width = "180px"><b>Action</b></th>
		</tr>
		
		@foreach ($book as $book)
		<tr>
			<td><b>{{++$i}}</b></td>
			<td>{{$book->name}}</td>
			<td>{{$book->isbn}}</td>
			<td>{{$book->details}}</td>
		<td>
			<form action="{{ route('book.destroy', $book->id) }}" method="post">
				
				<a class="btn btn-sm btn-warning" href="{{route('book.edit', $book->id)}}">Edit</a>
				@csrf
				
				<button type="submit" class="btn btn-sm btn-danger">Delete</button>
			</form>
		</td>

		</tr>

		@endforeach
	</table>




</div>



@endsection