@extends('layouts.app')
@section('content')

<div class="container">
	<div class="row">
		<div class="col-lg-120">
			<h3>New Book</h3>
			
		</div>
	</div>

	@if ($errors->any())
	<div class="alert alert-danger">
		<strong>Oops</strong> You forgot something.<br>
		<ul>
			@foreach ($errors as $error)
			<li>{{$error}}</li>
			@endforeach
		</ul>
	</div>
	@endif

	

	<form action="{{route('book.store')}}" method="post">
		@csrf
		<div class="row">
			<div class="col-md-12">
				<strong>Name:</strong>
				<input type="text" name="name" class="form-control" placeholder="Name">
			</div>
		</div>

			<div class="col-md-12">
				<strong>ISBN:</strong>
				<input type="text" name="isbn" class="form-control" placeholder="isbn">
			</div>
		

			<div class="col-md-12">
				<strong>Details:</strong>
				<textarea class="form-control" placeholder="Details" name="details"></textarea>
			</div>

			<div class="col-md-17">
				<a href="{{route('book.index')}}" class="btn btn-sm btn-success">Back</a>
				<button type="submit" class="btn btn-sm btn-primary">Submit</button>
			</div>
		</div>

	</form>
</div>


@endsection